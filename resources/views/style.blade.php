<?= '<?xml version="1.0" encoding="UTF-8"?>' ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:s="http://www.sitemaps.org/schemas/sitemap/0.9"
    xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" xmlns:video="http://www.google.com/schemas/sitemap-video/1.1"
    exclude-result-prefixes="s">
    <xsl:template match="/">
        <html>
        <head>
            <meta charset="utf-8" />
            <title>{{config('app.name')}} 网站地图</title>
            <style type="text/css">
                body {
                    font-family: Helvetica, Arial, sans-serif;
                    font-size: 13px;
                    color: #545353;
                }

                table {
                    border: none;
                    border-collapse: collapse;
                    margin: 1.5em auto;
                }

                tr.odd {
                    background-color: #eee;
                }

                tr:hover {
                    background-color: #ccc;
                }
                tr:hover td,
                tr:hover td a {
                    color: #000;
                }

                .expl {
                    margin: 10px 3px;
                    line-height: 1.3em;
                }

                a {
                    color: #da3114;
                    font-weight: bold;
                    text-decoration: none;
                }
                a:hover {
                    text-decoration: underline;
                }

                td {
                    font-size: 11px;
                }

                th {
                    text-align: left;
                    padding: 5px 20px 5px 5px;
                    font-size: 12px;
                    border-bottom: 1px solid #dedede;
                    cursor: pointer;
                }

                .url:hover {
                    cursor: pointer;
                    text-decoration: underline;
                }
            </style>
        </head>

        <body style="width:100%; text-align:center;">
            <div>
                <h2>{{config('app.name')}} 网站地图</h2>
                <p class="expl">
                    本页面由 <a href="/">{{config('app.name')}}网站</a> 自动生成，
                    共包含 <xsl:value-of select="count(s:urlset/s:url)"/> 条连接。
                </p>
                <table class="tablesorter" border="1" cellpadding="3">
                    <thead>
                        <tr bgcolor="#9acd32">
                            <th style="text-align:left">URL</th>
                            <th style="text-align:left">Priority</th>
                            <th style="text-align:left">Update freq</th>
                            <th style="text-align:left">Updated at</th>
                        </tr>
                    </thead>
                    <tbody>
                        <xsl:for-each select="s:urlset/s:url">
                            <tr>
                                <td class="url">
                                    <xsl:value-of select="s:loc" />
                                </td>
                                <td>
                                    <xsl:value-of select="s:priority" />
                                </td>
                                <td>
                                    <xsl:value-of select="s:changefreq" />
                                </td>
                                <td>
                                    <xsl:value-of select="concat(substring(s:lastmod,0,11),concat(' ', substring(s:lastmod,12,5)))"/>
                                </td>
                            </tr>
                        </xsl:for-each>
                    </tbody>
                </table>
            </div>
            <script type="text/javascript" src="https://cdn.staticfile.org/jquery/1.12.4/jquery.min.js"></script>
            <script type="text/javascript" src="https://cdn.staticfile.org/jquery.tablesorter/2.31.1/js/jquery.tablesorter.min.js"></script>
            <script type="text/javascript"><![CDATA[
                $(document).ready(function () {
                    $(".tablesorter").tablesorter({ sortList: [[3, 1], [1, 1]], widgets: ['zebra'] });
                    $('.url').click(function () {
                        window.open($(this).html());
                        return false;
                    });
                }); ]]>
            </script>
        </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
