<?php

namespace SC\Sitemap\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;

use SC\Sitemap\Facades\Sitemap;


class CacheCommand extends Command
{
    protected $signature = 'sitemap:cache';
    protected $description = 'Generate sitemap cache';

    public function handle()
    {
        Cache::forget(Sitemap::cacheKey());

        Sitemap::render();
        $this->info('cache sitemap successful');
    }
}
