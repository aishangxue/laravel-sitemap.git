<?php
namespace SC\Sitemap;

use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\AliasLoader;


class SitemapServiceProvider extends ServiceProvider
{
    public function register()
    {
        AliasLoader::getInstance()->alias('Sitemap', '\SC\Sitemap\Facades\Sitemap');
        if ($this->app->runningInConsole()) {
            $this->commands([
                Console\InstallCommand::class,
                Console\ClearCommand::class,
                Console\CacheCommand::class,
            ]);
        }

        $this->registerRoutes();
    }

    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'sitemap');

        $this->publishes([
            __DIR__.'/../publishes/' => public_path('vendor/sitemap'),
        ]);
    }

    public function registerRoutes()
    {
        app('router')->group([
            'namespace' => 'SC\Sitemap\Controllers',
            'as' => 'sitemap.',
        ], function ($router) {
            $router->get('/sitemap', 'SitemapController@index')->name('index');
            $router->get('/sitemap/style.xsl', 'SitemapController@style')->name('style');
        });
    }
}